from setuptools import setup, find_packages

setup(
    name="pyroils",
    description="Submit data to the Radiation Oncology Incident Learning System with Python and Selenium",
    version="0.2",
    author="Dustin Jacqmin, PhD",
    author_email="jacqmin+humanswillremovethis@humonc.wisc.edu",
    url="https://git.doit.wisc.edu/JACQMIN/pyroils",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    package_data={"pyroils": ["roils_spec/*.json"]},
)
