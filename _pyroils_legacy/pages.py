from pyroils.elements import (
    UsernameTextElement,
    PasswordTextElement,
    LocationMainDropdownElement,
    LocationSubDropdownElement,
    LocationAddtDropdownElement,
    ClassificationDropdownElement,
    NarrativeTextboxElement,
    TxTech2DCheckboxElement,
    TxTechElectronsCheckboxElement,
    TxTechRadiopharmCheckboxElement,
    TxTech3DCheckboxElement,
    TxTechIntraopCheckboxElement,
    TxTechTBICheckboxElement,
    TxTechIMRTCheckboxElement,
    TxTechKVCheckboxElement,
    TxTechNACheckboxElement,
    TxTechStereoCheckboxElement,
    TxTechLDRCheckboxElement,
    TxTechOtherCheckboxElement,
    TxTechParticlesCheckboxElement,
    TxTechHDRCheckboxElement,
    TxTechOtherTextboxElement,
    LocalIDTextboxElement,
    ReporterTextboxElement,
    DateTextboxElement,
    HourDropdownElement,
    MinuteDropdownElement,
    PeriodDropdownElement,
    SubmittedCheckboxElement,
    ClosedCheckboxElement,
    SubmittedStartDateTextElement,
    SubmittedEndDateTextElement,
    ReviewCompleteCheckboxElement,
    ReviewIncompleteCheckboxElement,
    EventNumberTextElement,
    LocalIdentifierTextElement,
    MyReviewLocationMainDropdownElement,
    MyReviewLocationSubDropdownElement,
    EventTypeDropdownElement,
)
from pyroils.locators import (
    LoginPageLocators,
    MainPageLocators,
    SelectFormPageLocators,
    SubmitEventPageLocators,
    ReviewSelectFormPageLocators,
    MyReviewSearchPageLocators,
)
import datetime


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class LoginPage(BasePage):
    """Methods for interacting with the RO-ILS login page"""

    username_text_element = UsernameTextElement()
    password_text_element = PasswordTextElement()

    def has_correct_title(self):
        """Verifies that the hardcoded text "Login" appears in page title"""
        return "Login" in self.driver.title

    def click_sign_in_button(self):
        """Clicks "Sign in" to login to the website"""

        element = self.driver.find_element(*LoginPageLocators.SIGN_IN_BUTTON)
        element.click()

    @staticmethod
    def is_login_page(driver):
        """ Checks page title of driver to see if it is on the login page """
        return "Login" in driver.title


class MainPage(BasePage):
    """Methods for interacting with the RO-ILS main page"""

    def has_correct_title(self):
        """Verifies that the hardcoded text "Healthcare SafetyZone Portal – Home" appears in page title"""
        return "Healthcare SafetyZone Portal – Home" in self.driver.title

    def click_submit_event_button(self):
        """Clicks "Submit Event" to move to the "Select Form" page """

        element = self.driver.find_element(*MainPageLocators.SUBMIT_EVENT_BUTTON)
        element.click()

    def click_my_review_event_button(self):
        """Clicks "My Review" to move to the "My Review Form Select" page """

        element = self.driver.find_element(*MainPageLocators.MY_REVIEW_BUTTON)
        element.click()

    @staticmethod
    def is_main_page(driver):
        """Verifies that the hardcoded text "Healthcare SafetyZone Portal – Home" appears in page title"""
        return "Healthcare SafetyZone Portal – Home" in driver.title


class SelectFormPage(BasePage):
    """Methods for interacting with the RO-ILS Select Form page"""

    def click_event_form_button(self):
        """Clicks "Event Form" to login to the website"""

        element = self.driver.find_element(*SelectFormPageLocators.EVENT_FORM_BUTTON)
        element.click()


class ReviewSelectFormPage(BasePage):
    """Methods for interacting with the RO-ILS Review Select Form page"""

    def click_select_button(self):
        """Clicks "Select" button"""

        element = self.driver.find_element(*ReviewSelectFormPageLocators.SELECT_BUTTON)
        element.click()


class MyReviewSearchPage(BasePage):
    """Methods for interacting with the RO-ILS My Review Search Page"""

    # Add page elements
    submitted_checkbox_element = SubmittedCheckboxElement()
    closed_checkbox_element = ClosedCheckboxElement()
    submitted_start_date_text_element = SubmittedStartDateTextElement()
    submitted_end_date_text_element = SubmittedEndDateTextElement()
    review_complete_checkbox_element = ReviewCompleteCheckboxElement()
    review_incomplete_checkbox_element = ReviewIncompleteCheckboxElement()
    event_number_text_element = EventNumberTextElement()
    local_identifier_text_element = LocalIdentifierTextElement()
    location_main_dropdown_element = MyReviewLocationMainDropdownElement()
    location_sub_dropdown_element = MyReviewLocationSubDropdownElement()
    event_type_dropdown_element = EventTypeDropdownElement()

    def click_reset_button(self):
        """Clicks the "Reset" button"""

        element = self.driver.find_element(*MyReviewSearchPageLocators.RESET_BUTTON)
        element.click()

    def click_search_button(self):
        """Clicks the "Search" button"""

        element = self.driver.find_element(*MyReviewSearchPageLocators.SEARCH_BUTTON)
        element.click()

    def enter_search_parameters(
        self,
        search_parameters=None,
        submitted_checkbox=True,
        closed_checkbox=False,
        submitted_start_date=datetime.datetime(2019, 1, 1),
        submitted_end_date=None,
        event_review_complete=False,
        event_review_incomplete=True,
        event_number=None,
        local_identifier=None,
        location=None,
        sub_location=None,
        event_type=None,
    ):
        """Enters search parameters on the My Review page

        PARAMETERS
        ----------
        search_parameters : dict
            A dictionary of serach parameters. If params is not None, the function
            will unpack the dictionary and update the keyword values to the values in
            the dictionary. The dictionary keys must have the same names as the function
            keywords (e.g. if you would like to set submitted_checkbox to True, parms
            will contain a key called "submitted_checkbox" with a value True).
        submitted_checkbox : bool
            Sets the value of the "Submitted" checkbox. Default is True.
        closed_checkbox : bool
            Sets the value of the "Closed" checkbox. Default is False.
        submitted_start_date : datetime.datetime
            Sets the value of the "Submitted Start Date" textbox. Default is 01/01/2019.
        submitted_end_date : datetime.datetime
            Sets the value of the "Submitted End Date" textbox. Default is None.
        event_review_complete : bool
            Sets the value of the Event Review "Complete" checkbox. Default is False.
        event_review_incomplete : bool
            Sets the value of the Event Review "Complete" checkbox. Default is True.
        event_number : str
            Sets the value of the "Event No" textbox. Default is None.
        local_identifier : string
            Sets the value of the "Local Identifier" textbox. Default is None.
        location : string
            Sets the value of the "Location" dropdown box. Default is None.
        sub_location : string
            Sets the value of the "Sub Location" dropdown box. Default is None.
        event_type : string
            Sets the value of the "Event Type" dropdown box, Default is None.
        """

        if search_parameters is not None:
            raise NotImplementedError(
                "The use of 'search_parameters' has not been implemented yet."
                )

        self.submitted_checkbox_element = submitted_checkbox
        self.closed_checkbox_element = closed_checkbox
        if submitted_start_date is not None:
            self.submitted_start_date_text_element = submitted_start_date.strftime("%m/%d/%Y")
        if submitted_end_date is not None:
            self.submitted_end_date_text_element = submitted_end_date.strftime("%m/%d/%Y")
        self.review_complete_checkbox_element = event_review_complete
        self.review_incomplete_checkbox_element = event_review_incomplete
        if event_number is not None:
            self.event_number_text_element = event_number
        if local_identifier is not None:
            self.local_identifier_text_element = local_identifier
        if location is not None:
            self.location_main_dropdown_element = location
        if sub_location is not None:
            self.location_sub_dropdown_element = sub_location
        if event_type is not None:
            self.event_type_dropdown_element = event_type

class SubmitEventPage(BasePage):
    """Methods for interacting with the RO-ILS Submit Event page"""

    # Add top-of-page elements
    location_main_element = LocationMainDropdownElement()
    location_sub_element = LocationSubDropdownElement()
    location_addt_element = LocationAddtDropdownElement()
    classification_element = ClassificationDropdownElement()
    narrative_element = NarrativeTextboxElement()

    # Add Treatment Technique elements
    tx_tech_2D_element = TxTech2DCheckboxElement()
    tx_tech_electrons_element = TxTechElectronsCheckboxElement()
    tx_tech_radiopharm_element = TxTechRadiopharmCheckboxElement()
    tx_tech_3D_element = TxTech3DCheckboxElement()
    tx_tech_intraop_element = TxTechIntraopCheckboxElement()
    tx_tech_TBI_element = TxTechTBICheckboxElement()
    tx_tech_IMRT_element = TxTechIMRTCheckboxElement()
    tx_tech_kV_element = TxTechKVCheckboxElement()
    tx_tech_NA_element = TxTechNACheckboxElement()
    tx_tech_stereo_element = TxTechStereoCheckboxElement()
    tx_tech_LDR_element = TxTechLDRCheckboxElement()
    tx_tech_other_element = TxTechOtherCheckboxElement()
    tx_tech_particles_element = TxTechParticlesCheckboxElement()
    tx_tech_HDR_element = TxTechHDRCheckboxElement()
    tx_tech_other_text_element = TxTechOtherTextboxElement()

    # Add end-of-page elements
    local_id_element = LocalIDTextboxElement()
    reporter_element = ReporterTextboxElement()
    date_element = DateTextboxElement()
    hour_element = HourDropdownElement()
    minute_element = MinuteDropdownElement()
    period_element = PeriodDropdownElement()

    def set_submit_form_elements(self, roils_data):
        """ Takes a ROILSData object and updates the submit form with the data.

        PARAMETERS
        ----------
        roils_data : pyroils.roilsdata.ROILSData
            A ROILSData object

        """
        # Extract the submission form data from the ROILSData object
        data = roils_data.get_event_data_as_hierarchical_dict()["submit_event_data"]

        # Set top-of-page elements
        if data["Location_Main"] is not None:
            self.location_main_element = data["Location_Main"]

        if data["Location_Sub"] is not None:
            self.location_sub_element = data["Location_Sub"]

        if data["Location_Addt"] is not None:
            self.location_addt_element = data["Location_Addt"]

        if data["Classification"] is not None:
            self.classification_element = data["Classification"]

        if data["Narrative"] is not None:
            self.narrative_element = data["Narrative"]

        # Set Treatment Technique checkbox elements
        if data["Tx_technique"]["2-D"] is not None:
            self.tx_tech_2D_element = data["Tx_technique"]["2-D"]
        if data["Tx_technique"]["3-D"] is not None:
            self.tx_tech_3D_element = data["Tx_technique"]["3-D"]
        if data["Tx_technique"]["IMRT/VMAT"] is not None:
            self.tx_tech_IMRT_element = data["Tx_technique"]["IMRT/VMAT"]
        if data["Tx_technique"]["SRS/SBRT"] is not None:
            self.tx_tech_stereo_element = data["Tx_technique"]["SRS/SBRT"]
        if data["Tx_technique"]["Particles (Protons)"] is not None:
            self.tx_tech_particles_element = data["Tx_technique"]["Particles (Protons)"]
        if data["Tx_technique"]["Electrons"] is not None:
            self.tx_tech_electrons_element = data["Tx_technique"]["Electrons"]
        if data["Tx_technique"]["Intraoperative"] is not None:
            self.tx_tech_intraop_element = data["Tx_technique"]["Intraoperative"]
        if (
            data["Tx_technique"]["kV x-rays (i.e. Orthovoltage and superficial)"]
            is not None
        ):
            self.tx_tech_kV_element = data["Tx_technique"][
                "kV x-rays (i.e. Orthovoltage and superficial)"
            ]
        if data["Tx_technique"]["LDR"] is not None:
            self.tx_tech_LDR_element = data["Tx_technique"]["LDR"]
        if data["Tx_technique"]["HDR"] is not None:
            self.tx_tech_HDR_element = data["Tx_technique"]["HDR"]
        if data["Tx_technique"]["Radiopharmaceuticals"] is not None:
            self.tx_tech_radiopharm_element = data["Tx_technique"][
                "Radiopharmaceuticals"
            ]
        if data["Tx_technique"]["Total body irradiation (TBI)"] is not None:
            self.tx_tech_TBI_element = data["Tx_technique"][
                "Total body irradiation (TBI)"
            ]
        if data["Tx_technique"]["Not Applicable"] is not None:
            self.tx_tech_NA_element = data["Tx_technique"]["Not Applicable"]
        if data["Tx_technique"]["Other"] is not None:
            self.tx_tech_other_element = data["Tx_technique"]["Other"]

        # If the Other" checkbox is checked, it makes the "Other" textbox visible.
        if self.tx_tech_other_element == True:
            if data["Tx_technique.Other_Text"] is not None:
                self.tx_tech_other_text_element = data["Tx_technique.Other_Text"]

        # Set bottom-of-page items
        if data["Local_ID"] is not None:
            self.local_id_element = data["Local_ID"]
        if data["Reporter_Name"] is not None:
            self.reporter_element = data["Reporter_Name"]

        # Decompose the time into pieces
        if data["Date_Time"] is not None:
            self.date_element = data["Date_Time"].strftime("%m/%d/%Y")

            hour = data["Date_Time"].hour
            if hour == 0:
                hour = 12
            elif hour > 12:
                hour = hour - 12

            self.hour_element = str(hour)

            self.minute_element = str(data["Date_Time"].minute)
            self.period_element = data["Date_Time"].strftime("%p")

    def click_save_button(self):
        element = self.driver.find_element(*SubmitEventPageLocators.SAVE_BUTTON)
        element.click()

    def click_reset_button(self):
        element = self.driver.find_element(*SubmitEventPageLocators.RESET_BUTTON)
        element.click()

    def click_cancel_button(self):
        element = self.driver.find_element(*SubmitEventPageLocators.CANCEL_BUTTON)
        element.click()

    def has_correct_title(self):
        """Verifies that the hardcoded text "Submit Event" appears in page title"""
        return "Submit Event" in self.driver.title

    @staticmethod
    def is_submit_event_page(driver):
        """Verifies that the hardcoded text "Submit Event" appears in page title"""
        return "Submit" in driver.title

