from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from pyroils.locators import (
    LoginPageLocators,
    SubmitEventPageLocators,
    ReviewSelectFormPageLocators,
    MyReviewSearchPageLocators,
)
import time


class BasePageTextElement(object):
    """Base text element class for setting and getting text from a text box"""

    locator = None

    def __set__(self, obj, value):
        """Sets the text to the value supplied"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        driver.find_element(*self.locator).clear()
        driver.find_element(*self.locator).send_keys(value)

    def __get__(self, obj, owner):
        """Gets the text of the specified object"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))

        element = driver.find_element(*self.locator)
        return element.get_attribute("value")


class BasePageHiddenTextElement(object):
    """Base text element class for setting and getting hidden text from
    elements with no textbox"""

    locator = None

    def __get__(self, obj, owner):
        """Gets the text of the specified object"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        element = driver.find_element(*self.locator)
        return element.get_attribute("value")


class BasePageCheckboxElement(object):
    """Base dropdown element class for setting and getting the value from a
    checkbox
    """

    locator = None

    def __set__(self, obj, value):
        """Sets the checkbox to the value supplied"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        checkboxElement = driver.find_element(*self.locator)

        # Check to see if the current state of the checkbox matches the
        # value passed by into the set method. If not, change the state
        if checkboxElement.is_selected() != value:
            checkboxElement.click()

        # Do it a second time, first one sometimes fails due to element not being active.
        if checkboxElement.is_selected() != value:
            checkboxElement.click()

        # Verify that the value assignment was successful
        assert (
            checkboxElement.is_selected() == value
        ), f"Checkbox value {checkboxElement.is_selected()} does not match the requested value {value}"

    def __get__(self, obj, owner):
        """Gets the value of of the specified checkbox"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        checkboxElement = driver.find_element(*self.locator)
        return checkboxElement.is_selected()


class BasePageDropdownElement(object):
    """Base dropdown element class for setting and getting text from a
    dropdown box
    """

    locator = None

    def __set__(self, obj, value):
        """Sets the dropdown box value to the value supplied"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        select = Select(driver.find_element(*self.locator))
        select.select_by_visible_text(value)

    def __get__(self, obj, owner):
        """Gets the text of the specified dropdown box"""
        driver = obj.driver
        WebDriverWait(driver, 10).until(EC.presence_of_element_located(self.locator))
        select = Select(driver.find_element(*self.locator))
        return select.first_selected_option.text


# LOGIN PAGE ELEMENTS


class UsernameTextElement(BasePageTextElement):
    """This class gets the text from the specified locator"""

    # The locator for search box where search string is entered
    locator = LoginPageLocators.USERNAME_TEXT_BOX


class PasswordTextElement(BasePageTextElement):
    """This class gets the text from the specified locator"""

    # The locator for search box where search string is entered
    locator = LoginPageLocators.PASSWORD_TEXT_BOX


# REVIEW SELECT FORM PAGE ELEMENTS
class FormSelectionDropdownElement(BasePageDropdownElement):

    locator = ReviewSelectFormPageLocators.FORM_SELECTION_DROPDOWN_BOX


# MY REVIEW SEARCH FORM


class SubmittedCheckboxElement(BasePageCheckboxElement):

    locator = MyReviewSearchPageLocators.SUBMITTED_CHECKBOX

class ClosedCheckboxElement(BasePageCheckboxElement):

    locator = MyReviewSearchPageLocators.CLOSED_CHECKBOX

class SubmittedStartDateTextElement(BasePageTextElement):

    locator = MyReviewSearchPageLocators.SUBMITTED_START_DATE_TEXTBOX

class SubmittedEndDateTextElement(BasePageTextElement):

    locator = MyReviewSearchPageLocators.SUBMITTED_END_DATE_TEXTBOX

class ReviewCompleteCheckboxElement(BasePageCheckboxElement):

    locator = MyReviewSearchPageLocators.REVIEW_COMPLETE_CHECKBOX

class ReviewIncompleteCheckboxElement(BasePageCheckboxElement):

    locator = MyReviewSearchPageLocators.REVIEW_INCOMPLETE_CHECKBOX

class EventNumberTextElement(BasePageTextElement):

    locator = MyReviewSearchPageLocators.EVENT_NUMBER_TEXTBOX

class LocalIdentifierTextElement(BasePageTextElement):

    locator = MyReviewSearchPageLocators.LOCAL_IDENTIFIER_TEXTBOX

class MyReviewLocationMainDropdownElement(BasePageDropdownElement):

    locator = MyReviewSearchPageLocators.MYREVIEW_LOCATION_MAIN_DROPDOWN_BOX

class MyReviewLocationSubDropdownElement(BasePageDropdownElement):

    locator = MyReviewSearchPageLocators.MYREVIEW_LOCATION_SUB_DROPDOWN_BOX

class EventTypeDropdownElement(BasePageDropdownElement):

    locator = MyReviewSearchPageLocators.EVENT_TYPE_DROPDOWN_BOX

# SUBMIT PAGE ELEMENTS


class LocationMainDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.LOCATION_MAIN_DROPDOWN_BOX


class LocationSubDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.LOCATION_SUB_DROPDOWN_BOX


class LocationAddtDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.LOCATION_ADDT_DROPDOWN_BOX


class ClassificationDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.CLASSIFICATION_DROPDOWN_BOX


class NarrativeTextboxElement(BasePageTextElement):

    locator = SubmitEventPageLocators.NARRATIVE_TEXT_BOX


class TxTech2DCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_2D_CHECKBOX


class TxTechElectronsCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_ELECTRONS_CHECKBOX


class TxTechRadiopharmCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_RADIOPHARM_CHECKBOX


class TxTech3DCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_3D_CHECKBOX


class TxTechIntraopCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_INTRAOP_CHECKBOX


class TxTechTBICheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_TBI_CHECKBOX


class TxTechIMRTCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_IMRT_CHECKBOX


class TxTechKVCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_KV_CHECKBOX


class TxTechNACheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_NA_CHECKBOX


class TxTechStereoCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_STEREO_CHECKBOX


class TxTechLDRCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_LDR_CHECKBOX


class TxTechOtherCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_OTHER_CHECKBOX


class TxTechParticlesCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_PARTICLES_CHECKBOX


class TxTechHDRCheckboxElement(BasePageCheckboxElement):

    locator = SubmitEventPageLocators.TX_TECH_HDR_CHECKBOX


class TxTechOtherTextboxElement(BasePageTextElement):

    locator = SubmitEventPageLocators.TX_TECH_OTHER_TEXT


class LocalIDTextboxElement(BasePageTextElement):

    locator = SubmitEventPageLocators.LOCAL_ID_TEXTBOX


class ReporterTextboxElement(BasePageTextElement):

    locator = SubmitEventPageLocators.REPORTER_TEXTBOX


class DateTextboxElement(BasePageTextElement):

    locator = SubmitEventPageLocators.DATE_TEXTBOX


class HourDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.HOUR_DROPDOWN


class MinuteDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.MINUTE_DROPDOWN


class PeriodDropdownElement(BasePageDropdownElement):

    locator = SubmitEventPageLocators.PERIOD_DROPDOWN
