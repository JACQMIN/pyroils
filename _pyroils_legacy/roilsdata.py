import pandas as pd


class ROILSData(object):
    """ Data for a single RO-ILS submission

    ATTRIBUTES
    ----------
    event_data : dict
        A dictionary containing the event data

    """

    _tx_technique_blank = {
        "2-D": None,  # bool
        "3-D": None,  # bool
        "IMRT/VMAT": None,  # bool
        "SRS/SBRT": None,  # bool
        "Particles (Protons)": None,  # bool
        "Electrons": None,  # bool
        "Intraoperative": None,  # bool
        "kV x-rays (i.e. Orthovoltage and superficial)": None,  # bool
        "LDR": None,  # bool
        "HDR": None,  # bool
        "Radiopharmaceuticals": None,  # bool
        "Total body irradiation (TBI)": None,  # bool
        "Not Applicable": None,  # bool
        "Other": None,  # bool
        "Other_Text": None,  # string
    }

    _submit_event_data_blank = {
        "Location_Main": None,  # string or pandas catagory
        "Location_Sub": None,  # string or pandas catagory
        "Location_Addt": None,  # string or pandas catagory
        "Classification": None,  # string or pandas catagory
        "Narrative": None,  # string
        "Tx_technique": _tx_technique_blank,  # dict
        "Local_ID": None,  # string
        "Reporter_Name": None,  # string
        "Date_Time": None,  # datetime
    }

    _discoverer_role_blank = {
        "Administrator": None,  # bool
        "Dosimetrist": None,  # bool
        "Nurse, NP or PA": None,  # bool
        "Patient or Patient Representative": None,  # bool
        "Physician": None,  # bool
        "Physicist": None,  # bool
        "Radiation Therapist": None,  # bool
        "Other": None,  # bool
        "Other_Text": None,  # string
    }

    _occurred_workflow_blank = {
        "Before Simulation": None,  # bool
        "Pre-planning Imaging and Simulation": None,  # bool
        "Treatment Planning": None,  # bool
        "Pre-Treatment QA Review (e.g. Physics Plan Check)": None,  # bool
        "Treatment Delivery including imaging (e.g. at the machine)": None,  # bool
        "On-Treatment QA (e.g. weekly check, physician OTV)": None,  # bool
        "After Treatment Course is Finished": None,  # bool
        "Equipment and Software QA": None,  # bool
        "Outside the Radiation Therapy Workflow or Other": None,  # bool
        "Other_Text": None,  # string
    }

    _imaging_blank = {
        "kV or MV radiographs": None,  # bool
        "kV or MV Cone-beam CT": None,  # bool
        "Ultrasound": None,  # bool
        "Electromagnetic Transponders": None,  # bool
        "Optical (surface) imaging": None,  # bool
        "MRI": None,  # bool
        "None": None,  # bool
        "Not Applicable": None,  # bool
        "Other": None,  # bool
        "Other_Text": None,  # string
    }

    _dose_deviation_blank = {
        "≤5% maximum dose deviation to target": None,  # bool
        ">5% but ≤25% maximum dose deviation totarget": None,  # bool
        ">25% but ≤100% maximum dose deviation to target": None,  # bool
        ">100% maximum dose deviation to target": None,  # bool
        "OAR(s) received more than intended but within tolerance levels": None,  # bool
        "OAR(s) received more than intended and exceeded tolerance": None,  # bool
        "Not applicable": None,  # bool
    }

    _reported_external_entities_blank = {
        "FDA": None,  # bool
        "NRC": None,  # bool
        "State": None,  # bool
        "Vendor/Manufacturer": None,  # bool
        "Other": None,  # bool
    }

    _my_review_data_blank = {
        "Title": None,  # string
        "Discoverer_Role": _discoverer_role_blank,  # dict
        "Patient_Age": None,  # string or pandas catagory
        "Patient_Gender": None,  # string or pandas catagory
        "Narrative_Supplemental": None,  # string
        "Discovered_ Descript": None,  # string
        "Discovered_Workflow": None,  # string
        "Occurred_Workflow": _occurred_workflow_blank,  # dict
        "Imaging": _imaging_blank,  # dict
        "Systematic_Error": None,  # bool or pandas catagory
        "Systematic_Patient_Number": None,  # int
        "Dose_Deviation": _dose_deviation_blank,  # dict
        "Fractions_Incorrect": None,  # int
        "Fractions_Total": None,  # int
        "Equipment_Related": None,  # bool
        "Equipment_Simulation": None,  # Not implemented, but will be dict
        "Equipment_Planning": None,  # Not implemented, but will be dict
        "Equipment_Management": None,  # Not implemented, but will be dict
        "Equipment_Photon_Electron": None,  # Not implemented, but will be dict
        "Equipment_Proton": None,  # Not implemented, but will be dict
        "Equipment_Other": None,  # Not implemented, but will be dict
        "Reported_PSO": None,  # bool
        "Reported_External": None,  # bool
        "Reported_External_Entities": _reported_external_entities_blank,  # dict
        "Significance_Scale": None,  # string or pandas catagory
        "Prevention_Ideas": None,  # string
        "Intervention": None,  # string
        "Additional_Details": None,  # string
        "Internal_Use": None,  # string
        "Status": None,  # bool
    }

    _contributing_factors_blank = {}

    _event_data_blank = {
        "submit_event_data": _submit_event_data_blank,  # dictionary
        "my_review_data": _my_review_data_blank,  # dictionary
        "contributing_factors": _contributing_factors_blank,  # probably a dictionary, but TBD
    }

    def __init__(self):
        self.event_data = ROILSData._flatten_dict(ROILSData._event_data_blank)

    def get_event_data_as_flattened_dict(self):
        return self.event_data

    def get_event_data_as_hierarchical_dict(self):
        return ROILSData._unflatten_dict(self.event_data)

    def get_qa_form_as_dataframe(self):
        fd = ROILSData._flatten_dict(self.event_data)
        df = pd.DataFrame(data=fd, index=[0])
        return df

    @staticmethod
    def _flatten_dict(d):
        def items():
            for key, value in d.items():
                if isinstance(value, dict):
                    for subkey, subvalue in ROILSData._flatten_dict(value).items():
                        yield key + ":" + subkey, subvalue
                else:
                    yield key, value

        return dict(items())

    @staticmethod
    def _unflatten_dict(d):
        resultDict = dict()
        for key, value in d.items():
            parts = key.split(":")
            d = resultDict
            for part in parts[:-1]:
                if part not in d:
                    d[part] = dict()
                d = d[part]
            d[parts[-1]] = value
        return resultDict
