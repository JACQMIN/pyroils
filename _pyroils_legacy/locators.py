from selenium.webdriver.common.by import By


class LoginPageLocators(object):
    """A class for RO-ILS login page locators. All login page locators should come here"""

    USERNAME_TEXT_BOX = (By.ID, "ctl00_contentBody_txtUserName")
    PASSWORD_TEXT_BOX = (By.ID, "ctl00_contentBody_txtPassword")
    SIGN_IN_BUTTON = (By.ID, "ctl00_contentBody_cmdSubmit")


class MainPageLocators(object):
    """A class for RO-ILS main page locators. All main page locators should come here"""

    SUBMIT_EVENT_BUTTON = (By.ID, "ctl00_contentBody_btnSubmit")
    MY_REVIEW_BUTTON = (By.ID, "ctl00_contentBody_Image3")
    ANALYSIS_BUTTON = (By.ID, "ctl00_contentBody_Image4")


class SelectFormPageLocators(object):
    """A class for RO-ILS Select Form page locators. All Select Form page locators should come here"""

    EVENT_FORM_BUTTON = (By.ID, "ctl00_contentBody_dlFormOne_ctl00_btnOne")
    DOCUMENT_UPLOAD_BUTTON = (By.ID, "ctl00_contentBody_dlFormTwo_ctl00_btnTwo")


class ReviewSelectFormPageLocators(object):
    """A class for RO-ILS Review Select Form page locators. """

    FORM_SELECTION_DROPDOWN_BOX = (By.ID, "ctl00_contentBody_ucSelectForm_cboForms")
    SELECT_BUTTON = (By.ID, "ctl00_contentBody_ucSelectForm_cmdSelect")


class MyReviewSearchPageLocators(object):
    """A class for manipulating the My Review search page"""

    SUBMITTED_CHECKBOX = (By.ID, "ctl00_contentBody_chkStatus_0")
    CLOSED_CHECKBOX = (By.ID, "ctl00_contentBody_chkStatus_1")
    SUBMITTED_START_DATE_TEXTBOX = (By.ID, "ctl00_contentBody_calStartDate_txtCalendar")
    SUBMITTED_END_DATE_TEXTBOX = (By.ID, "ctl00_contentBody_calEndDate_txtCalendar")
    REVIEW_COMPLETE_CHECKBOX = (By.ID, "ctl00_contentBody_chkEventReview_0")
    REVIEW_INCOMPLETE_CHECKBOX = (By.ID, "ctl00_contentBody_chkEventReview_1")
    EVENT_NUMBER_TEXTBOX = (By.ID, "ctl00_contentBody_txtIncidentNumber")
    LOCAL_IDENTIFIER_TEXTBOX = (By.ID, "ctl00_contentBody_txtMedicalRecord")
    MYREVIEW_LOCATION_MAIN_DROPDOWN_BOX = (By.ID, "ctl00_contentBody_ddlLocation")
    MYREVIEW_LOCATION_SUB_DROPDOWN_BOX = (By.ID, "ctl00_contentBody_ddlSubLocation")
    EVENT_TYPE_DROPDOWN_BOX = (By.ID, "ctl00_contentBody_ddlOccurrenceType")
    SEARCH_BUTTON = (By.ID, "ctl00_contentBody_cmdSearch")
    RESET_BUTTON = (By.ID, "ctl00_contentBody_btnReset")


class SubmitEventPageLocators(object):
    """ A class for form locators on the Submit Event page"""

    EVENT_NUMBER_TEXT = (By.ID, "hidEventNumber")
    LOCATION_MAIN_DROPDOWN_BOX = (
        By.ID,
        "ctl00_contentBody_rptQuestions_ctl01_cbodc4e6b5f-bbe5-11e1-8e08-002655348f00",
    )
    LOCATION_SUB_DROPDOWN_BOX = (
        By.ID,
        "ctl00_contentBody_rptQuestions_ctl02_cbodc4e6b62-bbe5-11e1-8e08-002655348f00",
    )
    LOCATION_ADDT_DROPDOWN_BOX = (
        By.ID,
        "ctl00_contentBody_rptQuestions_ctl03_cbodc4e6b63-bbe5-11e1-8e08-002655348f00",
    )
    CLASSIFICATION_DROPDOWN_BOX = (
        By.ID,
        "ctl00_contentBody_rptQuestions_ctl08_cbodc4e6b5e-bbe5-11e1-8e08-002655348f00",
    )
    NARRATIVE_TEXT_BOX = (By.ID, "ctl00_contentBody_rptQuestions_ctl12_ctl02")
    TX_TECH_2D_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f0053152e42-bd5f-11e2-a27a-002655348f00",
    )
    TX_TECH_ELECTRONS_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f0010d31dc2-02bd-11e4-a6dc-002655348f00",
    )
    TX_TECH_RADIOPHARM_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00c9dab23f-b757-11e2-a27a-002655348f00",
    )
    TX_TECH_3D_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00df49e3e0-7223-11e2-8c69-002655348f00",
    )
    TX_TECH_INTRAOP_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00288209bb-02bd-11e4-a6dc-002655348f00",
    )
    TX_TECH_TBI_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f0043983140-02bd-11e4-a6dc-002655348f00",
    )
    TX_TECH_IMRT_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00e759b2c1-7223-11e2-8c69-002655348f00",
    )
    TX_TECH_KV_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00451686e6-7224-11e2-8c69-002655348f00",
    )
    TX_TECH_NA_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f004c16f826-7224-11e2-8c69-002655348f00",
    )
    TX_TECH_STEREO_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00eeedb9c4-7223-11e2-8c69-002655348f00",
    )
    TX_TECH_LDR_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00237e22c9-7224-11e2-8c69-002655348f00",
    )
    TX_TECH_OTHER_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00bf838e09-fd63-11e3-a6dc-002655348f00",
    )
    TX_TECH_PARTICLES_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f00a724a835-b757-11e2-a27a-002655348f00",
    )
    TX_TECH_HDR_CHECKBOX = (
        By.ID,
        "ra85818d1-7223-11e2-8c69-002655348f0029f22275-7224-11e2-8c69-002655348f00",
    )
    TX_TECH_OTHER_TEXT = (By.ID, "ctl00_contentBody_rptQuestions_ctl18_ctl02")
    LOCAL_ID_TEXTBOX = (By.ID, "ctl00_contentBody_rptQuestions_ctl19_ctl02")
    REPORTER_TEXTBOX = (By.ID, "ctl00_contentBody_rptQuestions_ctl21_ctl02")
    DATE_TEXTBOX = (
        By.ID,
        "ctl00_contentBody_rptQuestions_ctl23_ctl02_ctl00_txtCalendar",
    )
    HOUR_DROPDOWN = (By.NAME, "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl01")
    MINUTE_DROPDOWN = (By.NAME, "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl02")
    PERIOD_DROPDOWN = (By.NAME, "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl03")

    SAVE_BUTTON = (By.ID, "ctl00_contentBody_btnSave")
    RESET_BUTTON = (By.ID, "ctl00_contentBody_btnReset2")
    CANCEL_BUTTON = (By.ID, "ctl00_contentBody_btnCancel")


class AnalysisPageLocators(object):
    """A class for RO-ILS Analysis page locators."""

    START_DATE_TEXT_BOX = (
        By.ID,
        "ctl00_adminContent_wizAnalysis_calStartDate_txtCalendar",
    )
    END_DATE_TEXT_BOX = (By.ID, "ctl00_adminContent_wizAnalysis_calEndDate_txtCalendar")
    NEXT_BUTTON = (By.ID, "ctl00_adminContent_btnNextTop")
    FORMS_DROPDOWN = (By.ID, "ctl00_adminContent_wizAnalysis_ucSelectForm_cboForms")
    EVENT_TYPE_SELECT_ALL_CHECKBOX = (By.ID, "chkSelectAllEventTypes")
    PRIMARY_LOCATIONS_SELECT_ALL_CHECKBOX = (By.ID, "chkSelectAllLocations")
    COLUMNS_BUTTON = (
        By.ID,
        "ctl00_adminContent_wizAnalysis_SideBarContainer_SideBarList_ctl07_SideBarButton",
    )
    EXPORT_TO_EXCEL_BUTTON = (
        By.ID,
        "ctl00_adminContent_wizAnalysis_SideBarContainer_SideBarList_ctl09_SideBarButton",
    )
    LIST_OF_AVAILABLE_COLUMNS_SELECTOR_BOX = (
        By.ID,
        "ctl00_adminContent_wizAnalysis_lstAvailableColumns",
    )
