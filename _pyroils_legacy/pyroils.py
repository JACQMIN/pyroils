

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from pyroils.pages import (
    LoginPage,
    MainPage,
    SelectFormPage,
    SubmitEventPage,
    ReviewSelectFormPage,
    MyReviewSearchPage,
)
import getpass
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class RoilsAutomator(object):
    """A RO-ILS data entry automator

    PARAMETERS
    ----------
    url : string
        The URL for your RO-ILS page
    username : str
        A username for log-in to RO-ILS (default is None)
    password : str
        A password for log-in to RO-ILS (default is None)
    headless : bool
        If available, settings this to True will open a headless browser (default is False)
    chrome_path : str
        The path to chromedriver.exe (default is None)

    ATTRIBUTES
    -----------
    driver : webdriver
        A selenium webdriver used for RO-ILS automation
    is_logged_in : bool
        True if the user is logged in

    """

    def __init__(
        self, url, username=None, password=None, headless=False, chrome_path=None
    ):
        self.is_logged_in = False
        self.url = url

        logger.info("Creating the RO-ILS webdriver")
        try:
            logger.debug("Trying to open Firefox")
            self.driver = webdriver.Firefox()
            logger.info("Firefox successfully opened")
        except WebDriverException:
            logger.debug("Trying to open Chrome")
            chrome_options = Options()
            if headless:
                chrome_options.add_argument("--headless")
            if chrome_path is not None:
                self.driver = webdriver.Chrome(chrome_path, options=chrome_options)
            else:
                self.driver = webdriver.Chrome(options=chrome_options)
            logger.info("Chrome successfully opened")
        self.driver.implicitly_wait(10)

        if username is not None and password is not None:
            self.login(username, password)

    def login(self, r_username, r_password):
        """Login to the RO-ILS website

        PARAMETERS
        ----------
        r_username : str
            Your RO-ILS username
        r_password : str
            Your RO-ILS password

        """

        if self.is_logged_in:
            logger.info(f"{r_username} is already logged in")
        else:
            logger.debug(f"Attempting login with username {r_username}")
            self.driver.get(self.url)
            if LoginPage.is_login_page(self.driver):
                login_page = LoginPage(self.driver)

                # Set username and password
                login_page.username_text_element = r_username
                login_page.password_text_element = r_password

                # Click the login button
                login_page.click_sign_in_button()

                # Wait until you arrive at the IssuePage
                wait = WebDriverWait(self.driver, 15)
                wait.until(EC.title_is("Healthcare SafetyZone Portal – Home"))

                self.is_logged_in = True
                logger.info(f"{r_username} has successfully logged in")

    def submit_event(self, roils_data, auto_save=False):
        """Takes a ROILSData object and completes the submission form

        PARAMETERS
        ----------
        roils_data : pyroils.roilsdata.ROILSData
            The event data to be submitted to RO-ILS. At a minimum, the
            "submit-event-data" dictionary should be completed
        auto_save: bool
            If true, the "Save" button will be clicked automatically after the form
            form elements are filled. The saving process may be unsuccessful if there
            are required fields that have not been completed. Default is False.

        """
        # Navigate to the submission form
        self._navigate_to_submission_form()

        # Pass the driver to the submit event page
        submit_event_page = SubmitEventPage(self.driver)
        submit_event_page.set_submit_form_elements(roils_data=roils_data)

        if auto_save:
            submit_event_page.click_save_button()

    def _navigate_to_submission_form(self):
        """ This private method navigates to the RO-ILS submission form """

        # Go to Main Page
        self.driver.get(self.url)

        # Click the Submit Event button
        main_page = MainPage(self.driver)
        main_page.click_submit_event_button()

        # Click the Event Form button
        select_form_page = SelectFormPage(self.driver)
        select_form_page.click_event_form_button()

    def enter_my_review_data(self, roils_data, local_identifier=None):
        """Takes a ROILSData object and completes the My Review form.

        PARAMETERS
        ----------
        roils_data : pyroils.roilsdata.ROILSData
            The event data to be submitted to RO-ILS. At a minimum, some of the
            "my-review-data" dictionary should be completed.
        local_identifier : str
            The local identifier associated with the event. If not explicitly entered by
            the user, the method will attempt to extract it from roils_data
        """

        if local_identifier is None:
            # Extract the local identifier from form data from the ROILSData object
            data = roils_data.get_event_data_as_hierarchical_dict()
            local_identifier = data["submit_event_data"]["Local_ID"]

        # Go to Main Page
        self.driver.get(self.url)

        # Click the My Review button
        main_page = MainPage(self.driver)
        main_page.click_my_review_event_button()

        # Click the Select button on the Review Select Form Page
        review_select_form_page = ReviewSelectFormPage(self.driver)
        review_select_form_page.click_select_button()

        # Complete the search form on the My Review Search page
        my_review_search_page = MyReviewSearchPage(self.driver)
        my_review_search_page.enter_search_parameters(
            submitted_checkbox=True,
            closed_checkbox=True,
            event_review_complete=True,
            event_review_incomplete=True,
            local_identifier=local_identifier,
            location=None,
            sub_location=None,
            event_type=None
        )
        my_review_search_page.click_search_button()
