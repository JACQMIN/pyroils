import pytest
import os
import datetime
from dotenv import load_dotenv
from pathlib import Path


env_path = Path("..") / ".env"
load_dotenv(dotenv_path=env_path)


@pytest.fixture()
def get_test_roils_data():

    from pyroils.pyroils import RoilsAutomator

    roils_dict = RoilsAutomator.get_empty_roils_dict()

    roils_dict["location-main"] = "University of Wisconsin Hospitals and Clinics"
    roils_dict["location-sub"] = "East Clinic"
    # Skipping roils_dict['location-addt'], always None
    roils_dict["event-classification"] = "Near-miss"
    roils_dict["narrative"] = "This is test data."

    roils_dict["tx-tech-2D"] = True
    roils_dict["tx-tech-electrons"] = True
    roils_dict["tx-tech-not-applicable"] = True
    roils_dict["tx-tech-LDR"] = True
    roils_dict["tx-tech-particles"] = True

    roils_dict["local-identifier"] = "1000"
    roils_dict["reporters-name"] = "Dustin Jacqmin, PhD"

    now = datetime.datetime.now()
    roils_dict["date-event-occurred"] = now.strftime("%m/%d/%Y")
    roils_dict["hour-event-occurred"] = str(int(now.strftime("%I")))
    roils_dict["minute-event-occurred"] = str(int(now.strftime("%M")))
    roils_dict["am-pm-event-occurred"] = now.strftime("%p")

    return roils_dict


@pytest.fixture(scope="module")
def automator_init(request):

    from pyroils.pyroils import RoilsAutomator

    username = os.getenv("ROILS_USERNAME")
    password = os.getenv("ROILS_PASSWORD")
    url = os.getenv("ROILS_URL")

    roils = RoilsAutomator(username=username, password=password, url=url)
    yield roils

    request.addfinalizer(roils.driver.close)


def test_login(automator_init):

    roils = automator_init
    assert roils.is_logged_in, "Login was not successful"


def test_submit_event(automator_init, get_test_roils_data):

    from pyroils.submit_page import SubmitEventPage

    roils = automator_init
    roils_dict = get_test_roils_data
    roils.submit_event(roils_dict)


@pytest.mark.skip(reason="This method is not implemented yet")
def test_my_review_data_entry(automator_init):

    local_identifier = 1000

    roils = automator_init
    roils_event = get_test_roils_data()
    roils.enter_my_review_data(
        roils_data=roils_event, local_identifier=local_identifier
    )
