import json

submit_event_dict = {
    "location-main": {
        "section": "submit-event",
        "text": "Location:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl01_cbodc4e6b5f-bbe5-11e1-8e08-002655348f00",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Location:"}},
    },
    "location-sub": {
        "section": "submit-event",
        "text": "Sub Location:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl02_cbodc4e6b62-bbe5-11e1-8e08-002655348f00",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Sub Location:"}},
    },
    "location-addt": {
        "section": "submit-event",
        "text": "Additional Location:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl03_cbodc4e6b63-bbe5-11e1-8e08-002655348f00",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Additional Location:"}},
    },
    "event-classification": {
        "section": "submit-event",
        "text": "Event Classification:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl08_cbodc4e6b5e-bbe5-11e1-8e08-002655348f00",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Event Classification:"}},
    },
    "narrative": {
        "section": "submit-event",
        "text": "Narrative: (Briefly describe the event, 4000 character limit):",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl12_ctl02",
            "element-type": "TEXTAREA",
        },
        "helium": {
            "type": "TextField",
            "params": {
                "label": "Narrative: (Briefly describe the event, 4000 character limit):"
            },
        },
    },
    "tx-tech-2D": {
        "section": "submit-event",
        "text": "2D",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f0053152e42-bd5f-11e2-a27a-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "2D"}},
    },
    "tx-tech-3D": {
        "section": "submit-event",
        "text": "3D",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00df49e3e0-7223-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "3D"}},
    },
    "tx-tech-IMRT-VMAT": {
        "section": "submit-event",
        "text": "IMRT/VMAT",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00e759b2c1-7223-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "IMRT/VMAT"}},
    },
    "tx-tech-SRS-SBRT": {
        "section": "submit-event",
        "text": "SRS/SBRT",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00eeedb9c4-7223-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "SRS/SBRT"}},
    },
    "tx-tech-particles": {
        "section": "submit-event",
        "text": "Particles (Protons)",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00a724a835-b757-11e2-a27a-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Particles (Protons)"}},
    },
    "tx-tech-electrons": {
        "section": "submit-event",
        "text": "Electrons",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f0010d31dc2-02bd-11e4-a6dc-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Electrons"}},
    },
    "tx-tech-intraoperative": {
        "section": "submit-event",
        "text": "Intraoperative",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00288209bb-02bd-11e4-a6dc-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Intraoperative"}},
    },
    "tx-tech-kV": {
        "section": "submit-event",
        "text": "kV x-rays (i.e. Orthovoltage and superficial)",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00451686e6-7224-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {
            "type": "CheckBox",
            "params": {"label": "kV x-rays (i.e. Orthovoltage and superficial)"},
        },
    },
    "tx-tech-LDR": {
        "section": "submit-event",
        "text": "LDR",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00237e22c9-7224-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "LDR"}},
    },
    "tx-tech-HDR": {
        "section": "submit-event",
        "text": "HDR",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f0029f22275-7224-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "HDR"}},
    },
    "tx-tech-radiopharmaceuticals": {
        "section": "submit-event",
        "text": "Radiopharmaceuticals",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00c9dab23f-b757-11e2-a27a-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Radiopharmaceuticals"}},
    },
    "tx-tech-TBI": {
        "section": "submit-event",
        "text": "Total body irradiation (TBI)",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f0043983140-02bd-11e4-a6dc-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {
            "type": "CheckBox",
            "params": {"label": "Total body irradiation (TBI)"},
        },
    },
    "tx-tech-not-applicable": {
        "section": "submit-event",
        "text": "Not Applicable",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f004c16f826-7224-11e2-8c69-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Not Applicable"}},
    },
    "tx-tech-other": {
        "section": "submit-event",
        "text": "Other",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ra85818d1-7223-11e2-8c69-002655348f00bf838e09-fd63-11e3-a6dc-002655348f00",
            "element-type": "CHECKBOX",
        },
        "helium": {"type": "CheckBox", "params": {"label": "Other"}},
    },
    "tx-tech-other-text": {
        "section": "submit-event",
        "text": "Specify 'Other' Treatment Technique:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl18_ctl02",
            "element-type": "TEXT",
        },
        "helium": {
            "type": "TextField",
            "params": {"label": "Specify 'Other' Treatment Technique:"},
        },
    },
    "local-identifier": {
        "section": "submit-event",
        "text": "Local Identifier:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl19_ctl02",
            "element-type": "TEXT",
        },
        "helium": {"type": "TextField", "params": {"label": "Local Identifier:"},},
    },
    "reporters-name": {
        "section": "submit-event",
        "text": "Reporter's Name:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl21_ctl02",
            "element-type": "TEXT",
        },
        "helium": {"type": "TextField", "params": {"label": "Reporter's Name:"},},
    },
    "date-event-occurred": {
        "section": "submit-event",
        "text": "Date and time the event occurred:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00_contentBody_rptQuestions_ctl23_ctl02_ctl00_txtCalendar",
            "element-type": "TEXT",
        },
        "helium": {
            "type": "TextField",
            "params": {"label": "Date and time the event occurred:"},
        },
    },
    "hour-event-occurred": {
        "section": "submit-event",
        "text": "Hour:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl01",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Hour:"}},
    },
    "minute-event-occurred": {
        "section": "submit-event",
        "text": "Min:",
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl02",
            "element-type": "DROPDOWN",
        },
        "helium": {"type": "ComboBox", "params": {"label": "Min:"}},
    },
    "am-pm-event-occurred": {
        "section": "submit-event",
        "text": None,
        "selenium": {
            "locator-by": "ID",
            "locator-value": "ctl00$contentBody$rptQuestions$ctl23$ctl02$ctl03",
            "element-type": "DROPDOWN",
        },
        "helium": {
            "type": "ComboBox",
            "params_eval": {"to_right_of": "ComboBox('Min:')"},
        },
    },
}

with open("submit_event_spec.json", "w") as f:
    json.dump(submit_event_dict, f)
