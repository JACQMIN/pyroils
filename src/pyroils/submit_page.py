""" submit_page.py

Copyright (C) 2020  Dustin Jacqmin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from helium import (
    set_driver,
    click,
    TextField,
    ComboBox,
    CheckBox,
    write,
    select,
    wait_until,
)
import logging
import json
from . import roils_spec
from time import sleep

# from roils_elements import ROILSWebElement


try:
    import importlib.resources as pkg_resources
except ImportError:
    # In python<3.7 fall-back to backported `importlib_resources`.
    import importlib_resources as pkg_resources

logger = logging.getLogger(__name__)

with pkg_resources.open_text(roils_spec, "submit_event_spec.json") as stream:
    submit_event_spec = json.load(stream)

helium_class_map = {"TextField": TextField, "ComboBox": ComboBox, "CheckBox": CheckBox}


class SubmitEventPage:
    """Methods for completing a new RO-ILS event submission"""

    # Class attributes
    title = "Submit Event"

    def __init__(self, driver):
        self.driver = driver
        set_driver(driver)

    def set_submit_event_elements(self, roils_data):
        """ Takes a RO-ILS data dictionary and updates the submit form 
        with the data.

        PARAMETERS
        ----------
        roils_data : dictionary
            A dictionary that contains RO-ILS data

        """

        verify_dict = {}

        for k, v in submit_event_spec.items():
            logger.debug(f"Setting element {k}")

            value = roils_data[k]

            if value is None:
                verify_dict[k] = None
                continue

            helium_dict = v["helium"]
            helium_type = helium_dict.get("type", None)
            params = helium_dict.get("params", {})
            params_eval = helium_dict.get("params_eval", {})

            for params_key, params_value in params_eval.items():
                params_eval[params_key] = eval(params_value)

            helium_class = helium_class_map[helium_type]
            helium_obj = helium_class(**params, **params_eval)

            if helium_type == "TextField":
                write(value, helium_obj)
                verify_dict[k] = helium_obj.value
            if helium_type == "ComboBox":
                select(helium_obj, value)
                verify_dict[k] = helium_obj.value
            if helium_type == "CheckBox":
                current_value = helium_obj.is_checked()
                while value != current_value:
                    click(helium_obj)
                    current_value = helium_obj.is_checked()

                verify_dict[k] = helium_obj.is_checked()

        # Verify correct input
        for k, v in verify_dict.items():
            assert (
                roils_data[k] == v
            ), f"Mismatch for key {k}: Input = {roils_data[k]}, Output = {v}"

    def click_save_button(self):
        """Click's the save button on the Submit Event page"""

        assert (
            self.is_submit_event_page()
        ), "Driver is not currently on the Submit Event page"

        click("Save")

        # It would be prudent to add additional logic to verify that the
        # save operation was successful and handle any issues

    def is_submit_event_page(self):
        return self.driver.title == self.title

    @staticmethod
    def get_empty_submit_event_dict():
        """ Returns an dictionary with all of the RO-ILS Submit Event data elements.

        RETURNS
        -------
        dict
            A dictionary whose keys are the RO-ILS Submit Event data elements. 
            The values are None.
        """
        empty_dict = {}

        for k, _ in submit_event_spec.items():
            empty_dict[k] = None

        return empty_dict
