""" pyqaforms

Copyright (C) 2020  Dustin Jacqmin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from helium import start_chrome, start_firefox, go_to, write, click, wait_until, S
from selenium.common.exceptions import WebDriverException
import getpass
import logging
from .submit_page import SubmitEventPage

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class RoilsAutomator(object):
    """A RO-ILS data entry automator

    PARAMETERS
    ----------
    url : string
        The URL for your RO-ILS page
    username : str
        A username for log-in to RO-ILS
    password : str
        A password for log-in to RO-ILS
    headless : bool
        If available, settings this to True will open a headless browser (default is False)

    ATTRIBUTES
    -----------
    driver : webdriver
        A selenium webdriver used for RO-ILS automation
    is_logged_in : bool
        True if the user is logged in

    """

    def __init__(self, url, username, password, headless=False):
        self.is_logged_in = False
        self.url = url

        logger.info("Creating a new webdriver")
        try:
            self.driver = start_firefox(headless=headless)
            logger.info("Successfully created a webdriver with Firefox")

        except WebDriverException:
            self.driver = start_chrome(headless=headless)
            logger.info("Successfully created a webdriver with Chrome")

        self.login(username, password)

    def login(self, r_username, r_password):
        """Login to the RO-ILS website

        PARAMETERS
        ----------
        r_username : str
            Your RO-ILS username
        r_password : str
            Your RO-ILS password

        """

        if self.is_logged_in:
            logger.info(f"{r_username} is already logged in")
        else:
            logger.debug(f"Attempting login with username {r_username}")
            go_to(self.url)
            write(r_username, "Username")
            write(r_password, "Password")
            click("Sign In")

            wait_until(
                lambda: self.driver.title == "Healthcare SafetyZone Portal – Home",
                timeout_secs=60,
            )

            self.is_logged_in = True
            logger.info(f"{r_username} has successfully logged in")

    def submit_event(self, roils_data, auto_save=False):
        """Takes a RO-ILS dictionary and completes the submission form

        PARAMETERS
        ----------
        roils_data : dict
            The event data to be submitted to RO-ILS. At a minimum, the
            Submit Event elements should be completed
        auto_save: bool
            If true, the "Save" button will be clicked automatically after the form
            form elements are filled. The saving process may be unsuccessful if there
            are required fields that have not been completed. Default is False.

        """

        # Verify login:
        logger.debug("Verifying login")
        assert self.is_logged_in, "You must be logged in to access QA Forms"

        # Navigate to the submission form
        self._navigate_to_submission_form()

        # Pass the driver to the submit event page
        submit_event_page = SubmitEventPage(self.driver)
        submit_event_page.set_submit_event_elements(roils_data=roils_data)

        if auto_save:
            submit_event_page.click_save_button()

    def _navigate_to_submission_form(self):
        """ This private method navigates to the RO-ILS submission form """

        # Go to Main Page
        go_to(self.url)

        click(S("#ctl00_contentBody_btnSubmit"))
        click("Event Form")

    def enter_my_review_data(self, roils_data, local_identifier=None):
        """Takes RO-ILS data and completes the My Review form.

        PARAMETERS
        ----------
        roils_data : dict
            The event data to be submitted to RO-ILS. At a minimum, the
            "My Review" elements should be completed
        local_identifier : str
            The local identifier associated with the event. If not explicitly entered by
            the user, the method will attempt to extract it from roils_data
        """
        pass

    @staticmethod
    def get_empty_roils_dict():
        """ Returns an dictionary with all of the RO-ILS data elements.

        RETURNS
        -------
        dict
            A dictionary whose keys are the RO-ILS data elements.
            The values are None.
        """

        submit_event_dict = SubmitEventPage.get_empty_submit_event_dict()

        return submit_event_dict
